<?php
    //Database params
    define("DB_HOST",'localhost');
    define("DB_USER",'root');
    define("DB_PASS",'');
    define("DB_NAME",'mvc_blog');

    /*Application root path*/
    define('APPROOT', dirname(__FILE__, 2));
    /*URLROOT*/
    define('URLROOT','http://localhost/php-starter-mvc');
    /*SITENAME*/
    define('SITENAME','PHP Starter MVC');
