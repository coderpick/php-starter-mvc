<?php 

Class Pages extends Controller {

    public $userModel;
    public function __construct() {
        //$this->userModel = $this->model('User');
    }
    public function index(){
        $data =[
          'title' =>"Home page",
          'siteName' =>'PHP Starter MVC Project'
        ];
        $this->view('pages/index',$data);
    }
    
    public function about(){
       $this->view('pages/about');
    }

    public function create()
    {
        echo "create page";
    }

}
