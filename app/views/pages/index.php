<?php require APPROOT . '/views/layouts/frontend/header.php'; ?>
<div class="mt-5 mb-5">
    <div class="container">
        <h1 class="display-4"><?php echo $data['siteName']?></h1>
        <p>This is a template for a simple marketing or informational website.
            It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.
        </p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a>
    </div>
</div>

<?php require APPROOT . '/views/layouts/frontend/footer.php'; ?>
